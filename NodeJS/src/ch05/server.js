// Module dependencies.
var application_root = __dirname,
	express = require( 'express' ), 	//Web framework
	path = require( 'path' ), 			//Utilities for dealing with file paths
	mongoose = require( 'mongoose' ); 	//MongoDB integration
	mongoose.connect('mongodb://localhost/libDB');//Connect to database

//Schemas
var Keywords = new mongoose.Schema({keyword: String}, { _id: false });// If you don't want an _id added to your schema at all, you may disable it using this option.
	
//Schemas
var Book = new mongoose.Schema({
	title: String,
	author: String,
	releaseDate: Date,
	keywords: [Keywords]
});



//Models
var BookModel = mongoose.model( 'books', Book );
	
/*var person = new Person({title: 'A', author: 'B', releaseDate: '2014-04-21'});
person.save(function(err) {
	if(err)
		console.log('error ocurr = ' + err);
});*/
	
/*BookModel.find({title: 'A'}, function(err, items) {
	items.forEach(function(data) {
		console.log(data);
	});
});*/

//Create server
var app = express();

// Configure server
app.configure( function(){
	
	//parses request body and populates request.body
	app.use( express.bodyParser() );

	//checks request.body for HTTP method overrides
	app.use( express.methodOverride() );
	
	//perform route lookup based on URL and HTTP method
	app.use( app.router );
	
	//Where to serve static content
	app.use( express.static( path.join( application_root, 'site') ) );
	
	//Show all errors in development
	app.use( express.errorHandler({ dumpExceptions: true, showStack: true }));
});

//Start server
var port = 4711;
app.listen( port, function()
{
	console.log( 'Express server listening on port %d in %s mode', port, app.settings.env );
});

app.get('/', function(req, res)
{
	  console.log("/ Get Default");
	  res.send();
});

app.get('/api', function( request, response )
{
	response.send( 'Library API is running' );
});

//Get a list of all books
app.get('/api/books', function( request, response )
{
	return BookModel.find(function(err, books) {
		if(!err) {
			console.log('books get succeed : ' + books.length);
			return response.send(books);
		} else {
			return console.log("GET ERROR : " + err);
		}
	});
});

//Insert a new book
app.post('/api/books', function( request, response )
{
	var book = new BookModel({
		title: request.body.title,
		author: request.body.author,
		releaseDate: request.body.releaseDate,
		keywords: request.body.keywords
	});
	
	//console.log(book.keywords);

	book.save(function(err) {
		if(!err) {
			return console.log('created');
		} else {
			return console.log("POST ERROR : " + err);
		}
	});

	return response.send( book );
});

//Get a single book by id
app.get('/api/books/:id', function( request, response )
{
	return BookModel.findById( request.params.id, function( err, book ) {
		if( !err ) {
			return response.send( book );
		} else {
			return console.log("GET BY ID ERROR : " + err);
		}
	});
});

//Update a book
app.put('/api/books/:id', function( request, response )
{
	console.log('Updating book ' + request.body.title );
	return BookModel.findById( request.params.id, function( err, book )
	{
		book.title = request.body.title;
		book.author = request.body.author;
		book.releaseDate = request.body.releaseDate;
		book.keywords = request.body.keywords;

		return book.save( function( err ) {
			if( !err ) {
				console.log( 'book updated' );
			} else {
				console.log("PUT ERROR : " + err);
			}
			return response.send( book );
		});
	});
});

//Delete a book
app.delete('/api/books/:id', function( request, response )
{
	console.log( 'Deleting book with id: ' + request.params.id );
	return BookModel.findById( request.params.id, function( err, book )
	{
		return book.remove( function( err ) {
			if( !err ) {
				console.log( 'Book removed' );
				return response.send( '' );
			} else {
				console.log( err );
			}
		});
	});
});