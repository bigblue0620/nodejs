/*function getData()
{
	// var xmlHttp = new XMLHttpRequest();
	// xmlHttp.open("GET", , false ); // false for synchronous request
    // xmlHttp.send( null );

    // return xmlHttp.responseText;

	// var deferred = $.Deferred();

    //or fetch api
    $.ajax({
        url : 'http://localhost:7779',
        success: function(data){
            deferred.resolve(data);
        },
        error: function(error) {
            deferred.reject(data);
        }
    });

    return deferred.promise();
}*/

// getData().then(function(value){
// 	$('.test').text(value);
// });

function getData()
{
    fetch('http://localhost:7779')
    .then(res => {
        console.log('response.status', res.status);
        console.log('response', res);
        console.dir(res.body);
        return res.json();
    })
    .then(data => {
        $('.test').text(data)
    });
}

getData();