const os = require('os-utils');
const cron = require('node-cron');
const app = require('express')();
var cors = require('cors')
let cpuUsage = 0;
var osu = require('node-os-utils')
var cpu = osu.cpu

cron.schedule('*/2 * * * * *', ()=>{
    // os.cpuUsage(function(v){
    //     console.log( 'CPU Usage (%): ' + v * 100 );
    //     cpuUsage = v * 100;
    // });
    cpu.usage().then(v => {
        console.log( 'CPU Usage (%): ' + v );
        cpuUsage = v;
    })
});

app.use(cors())

app.listen(7779, function() {
    console.log("===== Server Start =====");
});

app.get('/cpu', function(req, res)
{
    console.log("cpu get accessed");
    res.send(`${cpuUsage}`);
});