const app = require('express')();
const httpServer = require('http').createServer(app);
const io = require('socket.io')(httpServer);
const cron = require('node-cron');
const os = require('os');
//const osUtil = require('os-utils');
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
let serverIP = null;
 
let req = new XMLHttpRequest();
req.open('GET', 'https://ipinfo.io/ip');
req.send();
req.onreadystatechange = function(){
    if(req.readyState === 4 && req.status === 200){
        serverIP = (req.responseText).trim();
        console.log('external server ip =', serverIP);
        serverStart();
    }else{
        console.log('external server ip checking .....');
    }
};

function serverStart()
{
    httpServer.listen(7779, '127.0.0.1', null, () =>{
        console.log("===== Server Start =====");

        //console.log(os.networkInterfaces());
        //console.log(httpServer.address());
        console.log(os.cpus());
    });
}

io.on('connection', (socket) => {
  	
    console.log('connected socket ip =', socket.handshake.address);
  	console.log('connected socket.id =', socket.id);

    var data = {
        //hostname : os.hostname(),
        serverIP : serverIP,
    }

    io.emit('info', '' + JSON.stringify(data));

  	cron.schedule('*/2 * * * * *', ()=>{
        /*osUtil.cpuUsage((v)=>{
            io.emit('usage', 'CPU Usage (%): ' + v);
        });*/
    });

  	socket.on('disconnect', ()=>{
  		console.log('disconnect socket.id = ', socket.id);
  	});
});