var express = require('express');
var bodyParser = require('body-parser');
var cron = require('node-cron');
var gNum = 1;

var app = express();
	app.use(bodyParser.urlencoded({
    	extended: true
	}));
	app.use(bodyParser.json());

	app.listen(7779, function()
	{
		console.log("===== Server Start =====");
		cron.schedule('*/2 * * * * *', ()=>{
			gNum = (gNum > 100) ? 0 : ++gNum;
		});
	});

	app.get('/', function(req, res)
	{
		console.log("gNum = " + gNum);
		res.send(''+gNum);
	});

