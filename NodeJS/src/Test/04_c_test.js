//const socket = io.connect('http://110.11.180.189:7779');
const socket = io.connect('http://localhost:7779');

socket.on('info', (data)=>{
	$('.info').text(data);
});

socket.on('usage', (data)=>{
	$('.usage').text(data);
});

socket.on('disconnect', ()=>{
	console.log('disconnect ...');
	socket.disconnect(true); //TODO retry code
});