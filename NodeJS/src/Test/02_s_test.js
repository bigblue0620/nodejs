
/*const server = require('http').createServer();
const io = require('socket.io')(server);
io.on('connection', client => {
  client.on('event', data => {  });
  client.on('disconnect', () => {  });
});
server.listen(3000);*/

const app = require('express')();
const httpServer = require('http').createServer(app);
const io = require('socket.io')(httpServer);
const cron = require('node-cron');

let gNum = 0;

io.on('connection', (socket) => {
	//socket.emit('request', /* */); // emit an event to the socket
  	//io.emit('broadcast', /* */); // emit an event to all connected sockets
  	//socket.on('reply', function(){ /* */ }); // listen to the event
  	
  	console.log('connected socket.id = ', socket.id);
  	//socket.emit('data', '' + gNum);
  	cron.schedule('*/2 * * * * *', ()=>{
  		//gNum = Math.min(100, ++gNum);
  		console.log(socket.id, gNum);
  		//socket.emit('data', '' + gNum);
  		io.emit('data', '' + gNum);
	});

  	socket.on('disconnect', ()=>{
  		console.log('disconnect socket.id = ', socket.id);
  	})
});

httpServer.listen(7779, null, null, () =>{
	console.log("===== Server Start =====");
	cron.schedule('*/2 * * * * *', ()=>{
		gNum = (gNum > 100) ? 0 : ++gNum;
	});///2초마다는 */4
});