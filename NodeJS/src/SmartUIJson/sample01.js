/*http = require('http');
http.createServer(function (req, res) {
	res.writeHead(200, {'Content-Type' :'textplain'});
	res.write(JSON.stringify(Data));
	res.end();
}).listen(1337, '127.0.0.1');*/
	
var express = require('express');	
var app = express();
	app.use(express.bodyParser());
	
	app.post("/GetUsagePrnCnt", function(req, res)
	{
		console.log("Server accepted /GetUsagePrnCnt POST Request");
		//console.log(req);
		console.log(req.body);
		res.send(JSON.stringify(cntData));
	});
	
	app.post("/GetPrnListPolicy", function(req, res)
	{
		console.log("Server accepted /GetPrnListPolicy POST Request");
		console.log(req.body);
		//res.send(JSON.stringify(listData));
		res.send(JSON.stringify(ttt));
	});
	
	app.post("/RegisterCard", function(req, res)
	{
		console.log("Server accepted /RegisterCard POST Request");
		console.log(req.body);
		console.log(JSON.stringify(cardData));
		res.send(JSON.stringify(cardData));
	});
	
	app.post("/PrintAll", function(req, res)
	{
		console.log("Server accepted /PrintAll POST Request");
		console.log(req.body);
		console.log(JSON.stringify(printAllData));
		res.send(JSON.stringify(printAllData));
	});
	
	app.post("/PrintSelected", function(req, res)
	{
		console.log("Server accepted /PrintSelected POST Request");
		console.log(req.body);
		res.send(JSON.stringify(printSelectedData));
	});
	
	app.post("/DeleteSelected", function(req, res)
	{
		console.log("Server accepted /DeleteSelected POST Request");
		console.log(req.body);
		res.send(JSON.stringify(deletedData));
	});
	
	app.get("/", function(req, res)
	{
		console.log("Server accepted / Get Request");
		res.send("Server running...");
	});
	
	app.post("/test", function(req, res)
	{
		console.log("Server accepted /test POST Request");
		console.log(req.headers);
		res.send("test OK");
	});
	
	app.get("/test", function(req, res)
	{
		console.log("Server accepted /test Get Request");
		console.log(req.headers);
		//res.send("test OK");
		res.send(JSON.stringify(funcList));
	});
	
	app.get("/GetDeviceNativeMenuList", function(req, res)
	{
		console.log("Server accepted /GetDeviceNativeMenuList");
		//console.log(req.headers);
		//console.log(JSON.stringify(funcList));
		res.send(JSON.stringify(funcList));
	});
			
	app.listen(9009, function()
	{
		console.log("Server Start");
	});
	
var funcList = [
                [{key:"copy", name:{en:"copy", ko:"복사"}}], 
                [{key:"scan", name:{en:"scan", ko:"스캔"}}, {key:"scan_email", name:{en:"scan", ko:"스캔이메일"}}], 
                [{key:"fax", name:{en:"fax", ko:"팩스"}}]
];

/*var funcList = [
                [], 
                [{key:"scan", name:{en:"scan", ko:"스캔"}}, {key:"scan_email", name:{en:"scan", ko:"스캔이메일"}}], 
                []
];*/

var cntData = {
	result: {
    	functionCtrl : 0,
        limitColor : 999999,
        limitGray : 999999,
        overCountPrint : 'Y',
        prnCount : 3,
        usedColor : 0,
        usedGray: 0,
        userId : 'sec012'
    },
    status: 'success'
};
	
/*var cntData = {
			result: {
		    	
		    },
		    status: 'deviceIpNull'
}*/

var listData = {
		result : {
		        prnList: [
		            {
		                SRCnUp: 1,
		                color: "C",
		                destColor: "C",
		                docName: "TestFile1",
		                duplex: "S",
		                jobStatus: 0,
		                nUp: 1,
		                pageCnt: 1,
		                printCnt: 1,
		                printDate: "2015/05/08 16:31:52",
		                prnType: "N",
		                useYn:"N",
		                uuId:"F40ED107-FC4A-4545-A250-A52CB2ACFA30"
		            },
		            {
		                SRCnUp: 1,
		                color: "C",
		                destColor: "C",
		                docName: "TestFile2",
		                duplex: "S",
		                jobStatus: 0,
		                nUp: 1,
		                pageCnt: 1,
		                printCnt: 1,
		                printDate: "2015/05/08 16:30:57",
		                prnType: "N",
		                useYn: "N",
		                uuId: "1FC4299D-50C3-48CE-B8AA-FF0F1C84D40C"
		            },
		            {
		                SRCnUp: 1,
		                color: "C",
		                destColor: "C",
		                docName: "TestFile3",
		                duplex: "S",
		                jobStatus: 0,
		                nUp: 1,
		                pageCnt: 1,
		                printCnt: 1,
		                printDate: "2015/05/08 16:30:17",
		                prnType: "N",
		                useYn: "N",
		                uuId: "B8042ED7-AD76-4F87-A1AC-425486236709"
		            }
		        ],
		        userInfo : {
		            colorRestriction: "",
		            defaultBlack: "N",
		            forced2Up: "N",
		            forcedBlack: "N",
		            forcedDuplex: "N",
		            functionCtrl: "0",
		            printAgain: "N",
		            prnCount: "3",
		            userId: "sec012"
		        }
		    },
		    "status": "success"
}

var cardData = {
	status : "success"
}

var printAllData = {
	status : "fail",
	result : "limitCount|B8042ED7-AD76-4F87-A1AC-425486236709|1FC4299D-50C3-48CE-B8AA-FF0F1C84D40C"
}

var printSelectedData = {
	status : "success",
	result : ""
}

var deletedData = {
	status : "success",	
	queue : ""
}

var ttt = {
		result:
			{prnList:[
			          {"SRCnUp":1,
			        	  "color":"B",
			        	  "destColor":"B",
			        	  "docName":"Microsoft Word - 01.docx",
			        	  "duplex":"S",
			        	  "jobStatus":0,
			        	  "nUp":1,
			        	  "pageCnt":1,
			        	  "printCnt":1,
			        	  "printDate":"2015/06/03 17:41:27",
			        	  "prnType":"N",
			        	  "useYn":"N",
			        	  "uuId":"A8664B19-6F92-4F03-B276-D07F43E11C04"
			        	},
			        	{"SRCnUp":1,
			        		"color":"B",
			        		"destColor":"B",
			        		"docName":"Microsoft Word - 02.docx",
			        		"duplex":"S",
			        		"jobStatus":0,
			        		"nUp":1,
			        		"pageCnt":1,
			        		"printCnt":1,
			        		"printDate":"2015/06/03 18:14:59",
			        		"prnType":"N",
			        		"useYn":"N",
			        		"uuId":"48F58F61-5503-44AE-9F55-39886C29424D"
			 }],
			 userInfo:{
				 "colorRestriction":"",
				 "defaultBlack":"N",
				 "forced2Up":"N",
				 "forcedBlack":"N",
				 "forcedDuplex":"N",
				 "functionCtrl":0,
				 "printAgain":"N",
				 "prnCount":2,
				 "userId":"sec012"
			 }
		},
		"status":"success"
}