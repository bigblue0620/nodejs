// Module dependencies.
var application_root = __dirname,
	express = require( 'express' ), 	//Web framework
	path = require( 'path' );

var app = express();

// Configure server
app.configure( function(){
	
	//parses request body and populates request.body
	app.use( express.bodyParser() );

	//checks request.body for HTTP method overrides
	app.use( express.methodOverride() );
	
	//perform route lookup based on URL and HTTP method
	app.use( app.router );
	
	//Where to serve static content
	app.use( express.static( path.join( application_root, 'site') ) );
	
	//Show all errors in development
	app.use( express.errorHandler({ dumpExceptions: true, showStack: true }));
});

//Start server
var port = 1377;
app.listen( port, function()
{
	console.log( 'Express server listening on port %d in %s mode', port, app.settings.env );
});

app.get('/', function(req, res)
{
	  console.log("/ Get Default");
	  res.send();
});

app.get('/btest', function(request, response)
{
	console.log("/ Get btest");
	response.send(JSON.stringify(Data));
});

app.put('/btest/:id', function(request, response)
{
	console.log("/ Get btest id : " + request.params.id);
	response.send(JSON.stringify(request.body));
});

app.patch('/btest/:id', function(request, response)
{
	console.log("/ patch btest id : " + request.params.id);
	response.send(JSON.stringify(request.body));
});

app.delete('/btest/:id', function(request, response)
{
	console.log("/ DELETE btest id : " + request.params.id);
	response.send(JSON.stringify(request.body));
});


var Data = [
            {
                "id": "1",
                "title": "Sky",
                "completed": "false"
            },
            {
                "id": "2",
                "title": "Sea",
                "completed": "true"
            },
            /*{
                "id": "3",
                "title": "Mountain",
                "completed": "true"
            },*/
            {
                "id": "4",
                "title": "AirTest",
                "completed": "true"
            }
        ];