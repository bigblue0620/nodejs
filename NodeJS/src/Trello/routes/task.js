const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

//get all tasklist
router.get('/tasklist', (req, res) => {
    const model = require('../models/tasklist');
    model.find((err, list) => {
        if(err) {
            console.log('get all tasklist error', err);
            res.status(500).send(err);
        }

        const result = list.filter((el) => el.enable);
        res.send(result);
    });
});

// //get single tasklist
// app.get('/api/tasklist/:_id', (req, res) => {
//     res.end();
// });

//create single tasklist
router.post('/tasklist', (req, res) => {
    req.body._id = new mongoose.Types.ObjectId().toHexString();
    //console.log(req.body);
    const model = require('../models/tasklist');
    model.create(req.body, (err, raw) => {
        if(err) {
            console.log('create tasklist error', err);
            res.status(500).send(err);
        }

        res.send(raw);
    });
});

//update single tasklist
router.patch('/tasklist/:id', (req, res) => {
    //console.log('update tasklist name request body', req.body);
    const query = {"_id": req.body._id};
    const newValue = {"name": req.body.name};
    const model = require('../models/tasklist');

    model.updateOne(query, newValue, (err, raw) => {
        if(err){
            console.log('update tasklist error', err);
            res.status(500).send(err);
        }

        console.log('update tasklist name result', raw);
        res.send(true);
    });
});

//delete single tasklist
router.delete('/tasklist/:id', (req, res) => {
    //console.log('delete tasklist request body', req.body);
    const query = {"_id": req.body._id};
    const model = require('../models/tasklist');

    model.deleteOne(query, (err, raw) => {
        if(err){
            console.log('delete tasklist error', err);
            res.status(500).send(err);
        }

        console.log("delete tasklist result", raw);
        res.send(raw);
   });
});

//get all task item of single tasklist
router.get('/tasklist/:id/taskitem', (req, res) => {
    console.log('get task item of tasklist = ', req.params.id);
    const model = require('../models/taskitem');
    const query = {"_pid": req.params.id};

    model.find(query).sort([['order', 1]]).exec((err, list) => {
        if(err){
            console.log('get all tasklist error', err);
            res.status(500).send(err);
        }

        // const result = list.filter((el) => el.name);
        // res.send(result);
        res.send(list);
    });
});

//create taskitem
router.post('/tasklist/:id/taskitem', (req, res) => {
    req.body._id = new mongoose.Types.ObjectId().toHexString();
    //console.log(req.body);
    const model = require('../models/taskitem');
    model.create(req.body, (err, raw) => {
        if(err) {
            console.log('create taskitem error', err);
            res.status(500).send(err);
        }

        res.send(raw);
    });
});

//update single taskitem
router.patch('/tasklist/:id/taskitem/:id', (req, res) => {
    console.log('update single taskitem request body', req.body);
    const query = {"_id": req.body._id};
    const newValue = {[req.body.key] : req.body.value};
    const model = require('../models/taskitem');

    model.updateOne(query, newValue, (err, raw) => {
        if(err){
            console.log('update taskitem error', err);
            res.status(500).send(err);
        }

        //console.log('update taskitem result', raw);
        res.send(true);
    });
});

//delete single taskitem
router.delete('/tasklist/:id/taskitem/:id', (req, res) => {
    //console.log('delete tasklist request body', req.body);
    const query = {"_id": req.body._id};
    const model = require('../models/taskitem');

    model.deleteOne(query, (err, raw) => {
        if(err){
            console.log('delete taskitem error', err);
            res.status(500).send(err);
        }

        //console.log("delete taskitem result", raw);
        res.send(raw);
   });
});

//update all taskitem
router.put('/tasklist/:id/taskitem', (req, res) => {
    const model = require('../models/taskitem');
    const opt = req.body.opt;

    console.log('update all taskitem request body', JSON.stringify(opt));

    model.bulkWrite(opt, {"ordered": true, "w": 1}, (err, raw) => {
        if(err){
            console.log('update all tasklist error', err);
            res.status(500).send(err);
        }

        //console.log('update all task list result', raw);
        res.send(true);
    });
});

module.exports = router;