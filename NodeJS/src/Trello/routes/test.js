const express = require('express');
const router = express.Router();

const randomScalingFactor = () => {
    return (Math.random() > 0.5 ? 1.0 : -1.0) * Math.round(Math.random() * 100);
}

router.get('/random', (req, res) => {
    res.send('' + randomScalingFactor());
});

module.exports = router;