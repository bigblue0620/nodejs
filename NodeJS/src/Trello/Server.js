const mongoose = require('mongoose');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const taskRouter = require('./routes/task');
const testRouter = require('./routes/test');
const ws = require('ws').Server;
//const schedule = require('node-scheduler');

const app = express();
const wss = new ws({port: 9000});

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors());
app.use('/board', taskRouter);
app.use('/test', testRouter);
app.use('/', () => {});
app.listen(process.env.port || 8000);

wss.on('connection', (ws) => {
    console.log(`web socket clients count = ${wss.clients.size}`);
    setInterval(() => {
        ws.send((Math.random() > 0.5 ? 1.0 : -1.0) * Math.round(Math.random() * 100));
    }, 1000);
});

console.log('--------------- Server Running at Port 8000 ---------------');

mongoose.connect('mongodb://localhost:27017/test', {useNewUrlParser: true, useUnifiedTopology: true});
const db = mongoose.connection;
db.on('error', (err) => console.error('connection error: ', err));
db.once('open', () => console.log("mongo db connection OK."));