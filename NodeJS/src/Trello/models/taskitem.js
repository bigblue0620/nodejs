const mongoose = require('mongoose');

const schema = new mongoose.Schema(
    {
        _id: { type: String },
        _pid: { type: String },
        subject: { type: String },
        order: { type: Number }
    },
    {   versionKey: false   }
);

module.exports = mongoose.model('TaskItemModel', schema, 'taskitem');