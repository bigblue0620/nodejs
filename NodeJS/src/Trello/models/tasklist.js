const mongoose = require('mongoose');

const schema = new mongoose.Schema(
    {
        _id: { type: String },
        name: { type: String },
        enable: { type: Boolean }
    },
    {   versionKey: false   }
);

module.exports = mongoose.model('TaskModelModel', schema, 'tasklist');