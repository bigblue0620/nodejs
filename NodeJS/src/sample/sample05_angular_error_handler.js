const express = require('express');
const app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({
	extended : true
}));
app.use(bodyParser.json());

app.get('/', function(req, res)
{
	console.log("/ Get Default");
	res.send("OK");
});

app.post('/clientError', (req, res) => {
    console.log(req.body);
    res.send();
});

app.listen(65535, () => {
    console.log("------------- start -----------------");
});

console.log('Running at Port 65535');