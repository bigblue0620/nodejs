var http=require('http'),
	https = require('https'),
	express = require('express'),
	fs = require('fs'),
	bodyParser = require('body-parser'),
	cron = require('node-cron'),
	Response = require('http-response-object');	

var PROCESS_TYPES = {
	ADDRESS:"address"
};

//config.options.key = fs.readFileSync('key.pem');
//config.options.cert = fs.readFileSync('cert.pem');

var app = express();
//app.use(express.urlencoded());
app.use(bodyParser.urlencoded({
	extended : false
}));
app.use(bodyParser.json());
//http.createServer(app).listen(config.port1, function() {
//	console.log("Http server listening on port " + config.port1);
//});
http.createServer(app).listen(55030, function() {
	console.log("Https server listening on port " + 55030);
});

app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');  // ← コレを追加
	next();
  });

app.put('/library/:id', function (req, res)
{
	console.log("PUT");
	console.log("req.url : " + req.url);
	console.log("req.params.id : " + req.params.id);
	console.log("req.params : " + req.params);
	console.log("req.body : " + JSON.stringify(req.body));
	console.log("req.query : " + JSON.stringify(req.query));
		
	res.send();
});
	
app.get("/test", function(req, res)
{
	console.log("Server accepted /test/ GET Request");
	res.send(JSON.stringify(data));
});
	
app.get('/', function(req, res)
{
	console.log("/ Get Default");
	res.send("OK");
});

app.post("/login", function(req, res)
{
	console.log("Server Login POST Request");
	console.log(req.body);
	res.send(JSON.stringify(data));
});

app.get("/list", function(req, res)
{
	console.log("Server List Get Request");
	// setTimeout(function(){
	// 	res.send(JSON.stringify(mList));
	// }, 1000);
	res.send(JSON.stringify(mList));
});

app.post("/list", function(req, res)
{
	console.log("Server add user info request");
	let data = req.body;
	data.iSeq = mList.length + 1; 
	data.dtRegDate = new Date();
	mList.push(data);
	console.log(data);
	res.send();
});

app.put("/list/:id", function(req, res)
{
	console.log("PUT");
	console.log("req.url : " + req.url);
	console.log("req.params.id : " + req.params.id);
	//console.log("req.params : " + req.params);
	console.log("req.body : " + JSON.stringify(req.body));
	//console.log("req.query : " + JSON.stringify(req.query));
	var info = mList[parseInt(req.params.id)];
	console.log(info);
	info.vcPassword = req.body.vcPassword;
	info.vcName = req.body.vcName;
	info.vcEmailID = req.body.vcEmailID;
	info.vcEmailAddress = req.body.vcEmailAddress;
	info.vcPhone = req.body.vcPhone
	info.tiGrade = req.body.tiGrade;
	info.vcIP = req.body.vcIP;

	console.log(mList[parseInt(req.params.id)]);

	res.send();
});

app.delete("/list/:id", function(req, res){
	console.log('DELETE idx : ', req.params.id);
	mList.splice(req.params.id, 1);
	for(let m of mList){
		console.log(m.vcName);
	}
	res.send();
});

app.post("/user/login", function(req, res)
{
	console.log("Server Login POST Request");
	console.log(req.body);
	res.send(JSON.stringify(udata));
});

app.post("/member/register", function(req, res)
{
	console.log("member register POST Request");
	console.log(req.body);
	//res.send({success:true, data:{email:req.body.vcEmailID+'@'+req.body.vcEmailAddress, name:'이수민'}});
	res.send({success:true, data:{email:req.body.vcEmail, name:'이수민'}});
});

app.post("/member/auth", function(req, res)
{
	console.log("member register POST Request");
	console.log(req.body);
	//res.send({success:true, data:{email:req.body.vcEmailID+'@'+req.body.vcEmailAddress, name:'이수민'}});
	res.send({success:true, data:{email:'test@hanmail.net'}});
});

app.post('/api/Manager/Login', function(req, res)
{
	console.log("Server POST Request --- /api/Manager/Login");
	console.log(req.body);
	res.send(JSON.stringify({
		result: '000',
		message: 'OK',
		value: {}
	}));
});

app.post("/test/url", function(req, res)
{
	console.log("========== url extract ==========");
	res.send({data : "<html><head><script src='https://code.jquery.com/jquery-3.4.1.js' integrity='sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=' crossorigin='anonymous'></script></head><body><p>test</p><p>aaaa</p></body></html>"});
});

app.post("/api/account/EmailCheck", function(req, res)
{
	console.log("Server POST Request --- Email Duplicate Check");
	console.log(req.body);
	res.send(JSON.stringify(true));
});

app.post("/api/account/find/id", function(req, res)
{
	console.log("Server POST Request --- Find ID");
	console.log(req.body);
	res.send({data: {email:'swardking@hanmail.net', name:'tester'}});
});

app.post("/api/account/reset", function(req, res)
{
	console.log("Server POST Request --- Update PW");
	console.log(req.body);
	res.send({state:'ok', value:{}});
});

let udata = {biSeq:1, iPlatFormSeq:1, vcAccount:'test', vcName:'이준수', cBirthYear:'1990', cBirthMonth:'01', cBirthDay:'24', 
			tiGender:1, tiOrganization:1, dtRegDate:'2019-07-29 17:58:43', "isLogin" : true}

let data = 
{
    'iSeq': 1,
	"vcManagerID": "kyaaa",
	"vcName" : "kyong",
	"tiGrade" : 1,
	"isLogin" : true
}

let mList = 
[
	{iSeq:1, vcManagerID:'1bbbb', vcPassword:'bb', vcName:'관리자1', vcEmailID:'manager02', vcEmailAddress:'gmail.com', vcPhone:'01000010001', tiGrade:'super', vcIP:'127.0.0.1', dtRegDate:'2019-08-01 17:58:43', error:null},
	{iSeq:2, vcManagerID:'kang', vcPassword:'aa', vcName:'강현석', vcEmailID:'manager01', vcEmailAddress:'gmail.com', vcPhone:'01000020002', tiGrade:'super', vcIP:'127.0.0.1', dtRegDate:'2019-08-01 17:58:43', error:null},
	{iSeq:3, vcManagerID:'3cccc', vcPassword:'cc', vcName:'관리자3', vcEmailID:'manager03', vcEmailAddress:'gmail.com', vcPhone:'01000030003', tiGrade:'super', vcIP:'127.0.0.1', dtRegDate:'2019-08-01 17:58:43', error:null},
	{iSeq:4, vcManagerID:'4dddd', vcPassword:'dd', vcName:'관리자4', vcEmailID:'manager04', vcEmailAddress:'gmail.com', vcPhone:'01000040004', tiGrade:'super', vcIP:'127.0.0.1', dtRegDate:'2019-08-01 17:58:43', error:null},
	{iSeq:5, vcManagerID:'5eeee', vcPassword:'ee', vcName:'관리자5', vcEmailID:'manager05', vcEmailAddress:'gmail.com', vcPhone:'01000050005', tiGrade:'super', vcIP:'127.0.0.1', dtRegDate:'2019-08-01 17:58:43', error:null},
	{iSeq:6, vcManagerID:'6bbbb', vcPassword:'bb', vcName:'관리자6', vcEmailID:'manager02', vcEmailAddress:'gmail.com', vcPhone:'01000060006', tiGrade:'super', vcIP:'127.0.0.1', dtRegDate:'2019-08-01 17:58:43', error:null},
	{iSeq:7, vcManagerID:'7kang', vcPassword:'aa', vcName:'관리자7', vcEmailID:'manager01', vcEmailAddress:'gmail.com', vcPhone:'01000070007', tiGrade:'super', vcIP:'127.0.0.1', dtRegDate:'2019-08-01 17:58:43', error:null},
	{iSeq:8, vcManagerID:'8cccc', vcPassword:'cc', vcName:'관리자8', vcEmailID:'manager03', vcEmailAddress:'gmail.com', vcPhone:'01000080008', tiGrade:'super', vcIP:'127.0.0.1', dtRegDate:'2019-08-01 17:58:43', error:null},
	{iSeq:9, vcManagerID:'9dddd', vcPassword:'dd', vcName:'관리자9', vcEmailID:'manager04', vcEmailAddress:'gmail.com', vcPhone:'01000090009', tiGrade:'super', vcIP:'127.0.0.1', dtRegDate:'2019-08-01 17:58:43', error:null},
	{iSeq:10, vcManagerID:'10eeee', vcPassword:'ee', vcName:'관리자10', vcEmailID:'manager05', vcEmailAddress:'gmail.com', vcPhone:'01000100010', tiGrade:'super', vcIP:'127.0.0.1', dtRegDate:'2019-08-01 17:58:43', error:null},
	{iSeq:11, vcManagerID:'11bbbb', vcPassword:'bb', vcName:'관리자11', vcEmailID:'manager02', vcEmailAddress:'gmail.com', vcPhone:'01000110012', tiGrade:'super', vcIP:'127.0.0.1', dtRegDate:'2019-08-01 17:58:43', error:null},
	{iSeq:12, vcManagerID:'12kang', vcPassword:'aa', vcName:'강현석12', vcEmailID:'manager01', vcEmailAddress:'gmail.com', vcPhone:'01000120013', tiGrade:'super', vcIP:'127.0.0.1', dtRegDate:'2019-08-01 17:58:43', error:null},
	{iSeq:13, vcManagerID:'8cccc', vcPassword:'cc', vcName:'관리자13', vcEmailID:'manager03', vcEmailAddress:'gmail.com', vcPhone:'01000130013', tiGrade:'super', vcIP:'127.0.0.1', dtRegDate:'2019-08-01 17:58:43', error:null},
	{iSeq:14, vcManagerID:'9dddd', vcPassword:'dd', vcName:'관리자14', vcEmailID:'manager04', vcEmailAddress:'gmail.com', vcPhone:'01000140014', tiGrade:'super', vcIP:'127.0.0.1', dtRegDate:'2019-08-01 17:58:43', error:null},
	{iSeq:15, vcManagerID:'10eeee', vcPassword:'ee', vcName:'관리자15', vcEmailID:'manager05', vcEmailAddress:'gmail.com', vcPhone:'01000150015', tiGrade:'super', vcIP:'127.0.0.1', dtRegDate:'2019-08-01 17:58:43', error:null},
	{iSeq:16, vcManagerID:'11bbbb', vcPassword:'bb', vcName:'관리자16', vcEmailID:'manager02', vcEmailAddress:'gmail.com', vcPhone:'01000160016', tiGrade:'super', vcIP:'127.0.0.1', dtRegDate:'2019-08-01 17:58:43', error:null},
	{iSeq:17, vcManagerID:'12kang', vcPassword:'aa', vcName:'강현석17', vcEmailID:'manager01', vcEmailAddress:'gmail.com', vcPhone:'01000170017', tiGrade:'super', vcIP:'127.0.0.1', dtRegDate:'2019-08-01 17:58:43', error:null},
	{iSeq:18, vcManagerID:'9dddd', vcPassword:'dd', vcName:'관리자18', vcEmailID:'manager04', vcEmailAddress:'gmail.com', vcPhone:'01000180018', tiGrade:'super', vcIP:'127.0.0.1', dtRegDate:'2019-08-01 17:58:43', error:null},
	{iSeq:19, vcManagerID:'10eeee', vcPassword:'ee', vcName:'관리자19', vcEmailID:'manager05', vcEmailAddress:'gmail.com', vcPhone:'01000190019', tiGrade:'super', vcIP:'127.0.0.1', dtRegDate:'2019-08-01 17:58:43', error:null}	
];
