/*http = require('http');
http.createServer(function (req, res) {
	res.writeHead(200, {'Content-Type' :'textplain'});
	res.write(JSON.stringify(Data));
	res.end();
}).listen(1337, '127.0.0.1');*/
	
var express = require('express');
var bodyParser = require('body-parser');

var app = express();
	//app.use(express.bodyParser());
	app.use(bodyParser.urlencoded({
    	extended: true
	}));
	app.use(bodyParser.json());

	/*app.get("/btest/", function(req, res)
	{
		console.log("Server accepted /btest/ GET Request");
		res.send(JSON.stringify(Data));
	});
	
	app.put('/btest/:id', function (req, res)
	{
		console.log("PUT");
		console.log("req.url : " + req.url);
		console.log("req.params.id : " + req.params.id);
		console.log("req.params : " + req.params);
		console.log("req.body : " + JSON.stringify(req.body));
		console.log("req.query : " + JSON.stringify(req.query));

		for(var i=0; i<Data.length; i++){
			var item = Data[i];
			if(item.id == req.params.id){
				Data[i] = req.body;
				break;
			}
		}
		
		console.log(JSON.stringify(Data));
		res.send();
	});
	
	app.patch('/btest/:id', function (req, res)
	{
		console.log("PUT");
		console.log("req.url : " + req.url);
		console.log("req.params.id : " + req.params.id);
		console.log("req.params : " + req.params);
		console.log("req.body : " + JSON.stringify(req.body));
		console.log("req.query : " + JSON.stringify(req.query));
	});
	
	app.delete('/btest/:id', function(req, res)
	{
		console.log("DELETE");
		res.send();
	});*/
	
	app.put('/library/:id', function (req, res)
	{
		console.log("PUT");
		console.log("req.url : " + req.url);
		console.log("req.params.id : " + req.params.id);
		console.log("req.params : " + req.params);
		console.log("req.body : " + JSON.stringify(req.body));
		console.log("req.query : " + JSON.stringify(req.query));
		
		res.send();
	});
	
	app.get("/library/", function(req, res)
	{
		console.log("Server accepted /library/ GET Request");
		res.send(JSON.stringify(Data));
	});
	
	app.listen(1337, function()
	{
		console.log("server start");
	});
	
	app.get('/', function(req, res)
	{
		  console.log("/ Get Default");
		  res.send("OK");
	});

var Data = [
    {
        "id": "1",
        "title": "Sky",
        "completed": "false"
    },
    {
        "id": "2",
        "title": "Sea",
        "completed": "true"
    },
    {
        "id": "3",
        "title": "Mountain",
        "completed": "true"
    },
    {
        "id": "4",
        "title": "AirTest",
        "completed": "true"
    }
];