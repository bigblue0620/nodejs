(typeof define !== "function" ? function($){ $(require, exports, module); } : define)(function(require, exports, module, undefined) {
	"use strict";
	var btoa = require('btoa'); 
	var atob = require('atob'); 

	exports.encrypt = encrypt;
	exports.decrypt = decrypt;
	
	/**
	 * WG용 암호화 
	 * @param {string} msg 암호화할 문자열
	 * @param {string} key 키로 사용하는 문자열
	 * @return {string} 암호화된 문자열
	 */
	/*wg.encript = function(msg, key) {
	    var tmp, cArr=[];
	    var result = [];
	    var keyArr = [];
	    //처리를 편하게 하기위해 배열 번환후 리버스처리
	    var _keyArr = key.split('').reverse();
	    //Base64를 decoding 및 배열 변환 및 리버스처리
	    var msgArr = msg.split('').reverse();
	    for (var i = 0; i < msgArr.length; i++) {
	        //문자와 키의 코드값을 더함
	        cArr[cArr.length] = _keyArr[i % key.length];
	        cArr[cArr.length] = msg[i];
	        //tmp = msg[i].charCodeAt() + _keyArr[i % key.length].charCodeAt();
	        result[result.length] = msg[i].charCodeAt() + _keyArr[i % key.length].charCodeAt();
	        //코드값에 해당하는 문자열로 변환
	        //result[result.length] = String.fromCharCode(tmp);
	    }
	    console.log(cArr.reverse().join(''));
	    //결과값을 리버스
	    result.reverse();
	    //문자열로 변환후 Base64로 인코딩
	    //return btoa(result.join(''));
	    //return btoa(encodeURIComponent( escape( result.join('') )));
	    //return result.join('');
    	return String.fromCharCode.apply(this,result);
	};*/
	/**
	 * WG용 복호화 
	 * @param {string} msg 복호화할 문자열
	 * @param {string} key 키로 사용하는 문자열
	 * @return {string} 복호화된 문자열
	 */
	/*wg.decript = function(msg, key) {
	    var tmp, tmp2;
	    var result = [];
	    //처리를 편하게 하기위해 배열 번환후 리버스처리
	    var _keyArr = key.split('').reverse();
	    //Base64를 decoding 및 배열 변환 및 리버스처리
	    //var msgArr = atob(msg).split('').reverse();
	    var msgArr = unescape(decodeURIComponent(atob( msg ))).split('').reverse();
	
	    for (var i = 0; i < msgArr.length; i++) {
	        //암호화 문자열의 코드값
	        tmp = msgArr[i].charCodeAt();
	        //암호문자열에 포함된 키문자의 코드값 
	        tmp2 = _keyArr[i % key.length].charCodeAt();
	        //연산한 값을 원래 문자로 변환
	        result[result.length] = String.fromCharCode(tmp - tmp2);
	    }
	    //결과배열을 리버스
	    result.reverse();
	    return result.join('');
	}; */
	/**
	 * WG용 암호화 
	 * @param {string} msg 암호화할 문자열
	 * @param {string} key 키로 사용하는 문자열
	 * @return {string} 암호화된 문자열
	 */
	function encrypt(msg, key) {
	    var tmp, cArr=[];
	    var cMsg,cKey;
	    var result = [];
	    var keyArr = [];
	    //처리를 편하게 하기위해 배열 번환후 리버스처리
	    var _keyArr = key.split('');
	    //Base64를 decoding 및 배열 변환 및 리버스처리
	    var msgArr = msg.split('');
	    for (var i = 0; i < msgArr.length; i++) {
	        //문자와 키의 코드값을 더함
	        //cArr[cArr.length] = msg[i];
	        //cArr[cArr.length] = _keyArr[(i+key.length-1) % key.length];
	        cMsg = msg[i];
	        //cKey = _keyArr[(i+key.length-1) % key.length];
	        cKey = _keyArr[i % key.length];
	        //tmp = msg[i].charCodeAt() + _keyArr[i % key.length].charCodeAt();
	        tmp = cMsg.charCodeAt() + cKey.charCodeAt();
	        //result[result.length] = msg[i].charCodeAt() + _keyArr[(i+key.length-1) % key.length].charCodeAt();
	        //코드값에 해당하는 문자열로 변환
	        result[result.length] = String.fromCharCode(tmp);
	        //console.log("cMsg:"+cMsg+"/cKey:"+cKey+"/tmp:"+tmp+"/result::"+result[result.length-1]);
	    }
	    //console.log(cArr.join(''));
	    //결과값을 리버스
	    result.reverse();
	    //문자열로 변환후 Base64로 인코딩
	    //return btoa(result.join(''));
	    //return btoa(encodeURIComponent( escape( result.join('') )));
	    //return result.join('');
	    console.log(String.fromCharCode.apply(this,result));
    	return btoa(String.fromCharCode.apply(this,result));
	}
	/**
	 * WG용 복호화 
	 * @param {string} msg 복호화할 문자열
	 * @param {string} key 키로 사용하는 문자열
	 * @return {string} 복호화된 문자열
	 */
	function decrypt(msg, key) {
	    var tmp, tmp2;
	    var result = [];
	    //처리를 편하게 하기위해 배열 번환후 리버스처리
	    var _keyArr = key.split('').reverse();
	    //Base64를 decoding 및 배열 변환 및 리버스처리
	    var msgArr = atob(msg).split('').reverse();
	    //var msgArr = unescape(decodeURIComponent(atob( msg ))).split('').reverse();
	
	    for (var i = 0; i < msgArr.length; i++) {
	        //암호화 문자열의 코드값
	        tmp = msgArr[i].charCodeAt();
	        //암호문자열에 포함된 키문자의 코드값 
	        tmp2 = _keyArr[(i+key.length-1) % key.length].charCodeAt();
	        //연산한 값을 원래 문자로 변환
	        result[result.length] = String.fromCharCode(tmp - tmp2);
	    }
	    //결과배열을 리버스
	    result.reverse();
	    return result.join('');
	};
	
});
